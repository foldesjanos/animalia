import java.util.Date;

public class Consult {

    int idConsult;
    int idVeterinarian;
    int idPet;
    Date date;
    String description;

    public Consult(int idConsult, int idVeterinarian, int idPet, Date date, String description) {
        this.idConsult = idConsult;
        this.idVeterinarian = idVeterinarian;
        this.idPet = idPet;
        this.date = date;
        this.description = description;
    }
    
    public int getIdConsult() {
        return idConsult;
    }

    public void setIdConsult(int idConsult) {
        this.idConsult = idConsult;
    }

    public int getIdVeterinarian() {
        return idVeterinarian;
    }

    public void setIdVeterinarian(int idVeterinarian) {
        this.idVeterinarian = idVeterinarian;
    }

    public int getIdPet() {
        return idPet;
    }

    public void setIdPet(int idPet) {
        this.idPet = idPet;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
