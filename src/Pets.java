import java.sql.*;

public class Pets {

    int OwnerId;
    String race;
    boolean isVaccinated;
    Date date;

    public Pets(int ownerId, String race, boolean isVaccinated, Date date) {
        OwnerId = ownerId;
        this.race = race;
        this.isVaccinated = isVaccinated;
        this.date = date;
    }
}
