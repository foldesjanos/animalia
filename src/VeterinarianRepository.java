import java.sql.*;
import java.util.ArrayList;

public class VeterinarianRepository {


    public ArrayList<Veterinarian> findAll() {
        ArrayList<Veterinarian> veterinarianList = new ArrayList<>();
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* ALL VETERINARIANS ******************************************");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM animalia.veterinarian");
            while (rs.next()) {
                Integer veterinarianId = rs.getInt("veterinarianId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String veterinarianAddress = rs.getString("veterinarianAddress");
                String veterinarianSpeciality = rs.getString("veterinarianSpeciality");

                Veterinarian veterinarian = new Veterinarian();
                veterinarian.veterinarianId = veterinarianId;
                veterinarian.firstName = firstName;
                veterinarian.lastName = lastName;
                veterinarian.veterinarianAddress = veterinarianAddress;
                veterinarian.veterinarianSpeciality = veterinarianSpeciality;

                veterinarianList.add(veterinarian);
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return veterinarianList;
    }





    public Veterinarian findById(int veterinarianId) {
        Veterinarian veterinarian = new Veterinarian();
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* ALL VETERINARIANS ******************************************");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM animalia.veterinarian where veterinarianId = " + veterinarianId);
            while (rs.next()) {
                Integer vetId = rs.getInt("veterinarianId");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                String veterinarianAddress = rs.getString("veterinarianAddress");
                String veterinarianSpeciality = rs.getString("veterinarianSpeciality");

                veterinarian.veterinarianId = vetId;
                veterinarian.firstName = firstName;
                veterinarian.lastName = lastName;
                veterinarian.veterinarianAddress = veterinarianAddress;
                veterinarian.veterinarianSpeciality = veterinarianSpeciality;
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
        return veterinarian;
    }



    public void updateVeterinarian(String firstName, String lastName, String veterinarianAddress, String veterinarianSpeciality, int veterinarianId) {
//        nou
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* UPDATE VETERINARIAN ******************************************");
            String insertVeterinarian = "UPDATE `animalia`.`veterinarian` SET `firstName` = ?, `lastName` = ?, `veterinarianAddress` = ?, `veterinarianSpeciality` = ? WHERE (`veterinarianId` = ?)";
            PreparedStatement stmt = conn.prepareStatement(insertVeterinarian);

            stmt.setString(1,firstName);
            stmt.setString(2,lastName);
            stmt.setString(3,veterinarianAddress);
            stmt.setString(4,veterinarianSpeciality);
            stmt.setInt(5,veterinarianId);

            stmt.executeUpdate();
            stmt.close();
            conn.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }





    public void insertVeterinarian(String firstName, String lastName, String veterinarianAddress, String veterinarianSpeciality) {
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* INSERT VETERINARIAN ******************************************");
            String insertVeterinarian = "INSERT INTO `animalia`.`veterinarian` (`firstName`, `lastName`, `veterinarianAddress`, `veterinarianSpeciality`) VALUES (?, ?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(insertVeterinarian);

            stmt.setString(1,firstName);
            stmt.setString(2,lastName);
            stmt.setString(3,veterinarianAddress);
            stmt.setString(4,veterinarianSpeciality);

            stmt.executeUpdate();
            stmt.close();
            conn.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }


    public void deleteVeterinarian(int veterinarianId) {
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* DELETE VETERINARIAN ******************************************");
            String deleteVeterinarian = "DELETE FROM `animalia`.`veterinarian` WHERE (`veterinarianId` = ?)";
            PreparedStatement stmt = conn.prepareStatement(deleteVeterinarian);

            stmt.setInt(1,veterinarianId);

            stmt.executeUpdate();
            stmt.close();
            conn.close();
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}
