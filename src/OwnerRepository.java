import java.security.acl.Owner;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OwnerRepository {

    public List<Owners> findAll() throws SQLException {
        List<Owners> ownerList = new ArrayList<>();
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST,
                Constants.DATABASE_USERNAME,
                Constants.DATABASE_PASSWORD);
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM owners ");
        while (rs.next()) {
            int ownerId = rs.getInt("ownerId");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            Owners owners = new Owners(ownerId, firstName, lastName);
            ownerList.add(owners);
            System.out.println((ownerId + " " + firstName + " " + lastName));
        }
        rs.close();
        stmt.close();
        conn.close();
        return ownerList;
    }

    public void deleteById(int ownerId) throws SQLException {
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST,
                Constants.DATABASE_USERNAME,
                Constants.DATABASE_PASSWORD);
        Statement stmt = conn.createStatement();
        int rs = stmt.executeUpdate("DELETE FROM `animalia`.`owners` WHERE (`ownerId` = '" + ownerId + "')");

        stmt.close();
        conn.close();


    }

    public void insertInToOwners(String firstName, String lastName) throws SQLException {
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST,
                Constants.DATABASE_USERNAME,
                Constants.DATABASE_PASSWORD);

        String insertInToOwners = "INSERT INTO owners ( firstName, lastName) VALUES (?,?)";

        PreparedStatement stmt = conn.prepareStatement(insertInToOwners);
        stmt.setString(1, firstName);
        stmt.setString(2, lastName);
        stmt.executeUpdate();
    }

    public void modifyInToOwners(int ownerId, String firstName, String lastName) throws SQLException {
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST,
                Constants.DATABASE_USERNAME,
                Constants.DATABASE_PASSWORD);
        String modifyInToOwners = "UPDATE animalia.owners SET firstName=? ,lastName=? where ownerId=?";

        PreparedStatement stmt = conn.prepareStatement(modifyInToOwners);
        stmt.setString(1, firstName);
        stmt.setString(2, lastName);
        stmt.setInt(3, ownerId);
        stmt.executeUpdate();


    }


}

