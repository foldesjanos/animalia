import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ConsultRepository {

    List<Consult> consultList = new ArrayList<>();

    public List<Consult> findAll() throws SQLException {

        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST = "jdbc:mysql://localhost:3305/animalia",
                Constants.DATABASE_USERNAME = "root",
                Constants.DATABASE_PASSWORD = "root");

        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM consult ");

        while (rs.next()) {
            int idConsult = rs.getInt("consultId");
            int idVeterinarian = rs.getInt("veterinarianId");
            int idPet = rs.getInt("petId");
            Date date = rs.getDate("date");
            String description = rs.getNString("description");

            Consult consult = new Consult(idConsult, idVeterinarian, idPet, date, description);
            consultList.add(consult);

            // System.out.println((idConsult + "--" + idVeterinarian + "-" + idPet + "-"
            //         + date + ":" + description));
        }

        rs.close();
        stmt.close();
        conn.close();

        return consultList;
    }

    public void insertConsult(int veterinarianId, int petId, String date, String description) throws SQLException {
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST = "jdbc:mysql://localhost:3306/animalia",
                Constants.DATABASE_USERNAME = "root",
                Constants.DATABASE_PASSWORD = "root");

        //Statement stmt = conn.createStatement();
        //stmt.executeUpdate("INSERT INTO `animalia`.`consult` (veterinarianId,petId,date,description) VALUES " + veterinarianId + "," + petId + "," + date + "," + description +")");

        //INSERT INTO `animalia`.`consult` (`consultId`, `veterinarianId`, `petId`, `date`, `description`) VALUES ('', '4', '5', '6', '7');

        String insertVeterinarian = "INSERT INTO consult (veterinarianId,petId,date,description) VALUES (?,?,?,?)";
        PreparedStatement stmt = conn.prepareStatement(insertVeterinarian);
        stmt.setInt(1, veterinarianId);
        stmt.setInt(2, petId);
        stmt.setString(3, date);
        stmt.setString(4, description);
        stmt.executeUpdate();
        stmt.close();
        conn.close();
    }

    public void updateConsult(int veterinarianId, int petId, String date, String description, int consultId) throws SQLException {

        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST = "jdbc:mysql://localhost:3306/animalia",
                Constants.DATABASE_USERNAME = "root",
                Constants.DATABASE_PASSWORD = "root");

        String insertConsult = "UPDATE `animalia`.`consult` SET `veterinarianId` = ?, `petId` = ?, `date` = ?, `description` = ? WHERE (`consultId` = ?)";
        PreparedStatement stmt = conn.prepareStatement(insertConsult);
        stmt.setInt(1, veterinarianId);
        stmt.setInt(2, petId);
        stmt.setString(3, date);
        stmt.setString(4, description);
        stmt.setInt(5, consultId);
        stmt.executeUpdate();

        stmt.close();
        conn.close();
    }

    public void deleteConsult(int consultId) throws SQLException {
        Connection conn = DriverManager.getConnection(
                Constants.DATABASE_HOST = "jdbc:mysql://localhost:3306/animalia",
                Constants.DATABASE_USERNAME = "root",
                Constants.DATABASE_PASSWORD = "root");

        String deleteQuery = "DELETE FROM consult WHERE consultId =" + consultId;
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(deleteQuery);

        stmt.close();
        conn.close();

    }
}



