import java.sql.*;
import java.util.ArrayList;

public class PetsRepository {

    ArrayList<Pets> petsArrayList = new ArrayList<>();

    public ArrayList<Pets> findAll() {
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);
            System.out.println("************************************* All Pets ******************************************");
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM pets");
            while (rs.next()) {
                Integer ownerId = rs.getInt("ownerId");
                Date birthdate = rs.getDate("birthdate");
                boolean isVaccinated = rs.getBoolean("isVaccinated");
                String race = rs.getString("race");

                Pets pets = new Pets(ownerId, race, isVaccinated, birthdate) {
                };
                petsArrayList.add(pets);
                System.out.println((ownerId + "-" + birthdate + "-" + isVaccinated + "-" + "-" + race));

            }
            rs.close();
            stmt.close();
            conn.close();
            return petsArrayList;
        } catch (SQLException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());

        }
        return petsArrayList;
    }

    public void findById(int id) {
        System.out.println();
        System.out.println("Display pets find by ID");
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM pets WHERE ownerId = " + id);
            while (rs.next()) {
                Integer ownerId = rs.getInt("ownerId");
                Date birthdate = rs.getDate("birthdate");
                boolean isVaccinated = rs.getBoolean("isVaccinated");
                String race = rs.getString("race");
                System.out.println((ownerId + "-" + birthdate + "-" + isVaccinated + "-" + "-" + race));
            }
            stmt.close();
            rs.close();
            conn.close();

        } catch (SQLDataException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void addPetsById(int ownerId, String birthDate, boolean isVaccinated, String race) {
        System.out.println();
        System.out.println("Add pets");
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);

            Statement stmt = conn.createStatement();

            stmt.executeUpdate("INSERT INTO pets(ownerId, birthDate, isVaccinated, race) VALUES (" + ownerId + ", '" + birthDate + "', " + isVaccinated + ", '" + race + "')");
            ResultSet rs = stmt.executeQuery("SELECT * FROM pets");
            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLDataException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }

    public void deletePetsByOwnerId(int id) {
        System.out.println();
        System.out.println("Delete pets find by ownerID");
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);

            Statement stmt = conn.createStatement();
            int result = stmt.executeUpdate("DELETE FROM pets WHERE ownerId = " + id);
            ResultSet rs = stmt.executeQuery("SELECT * FROM pets");
            while (rs.next()) {
                Integer ownerId = rs.getInt("ownerId");
                System.out.println(ownerId);
            }
            stmt.close();
            rs.close();
            conn.close();

        } catch (SQLDataException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }

    }

    public void updatePetsById(int ownerId, String birthDate, boolean isVaccinated, String race, int petId) {
        System.out.println();
        System.out.println("Update pets");
        try {
            Connection conn = DriverManager.getConnection(
                    Constants.DATABASE_HOST,
                    Constants.DATABASE_USERNAME,
                    Constants.DATABASE_PASSWORD);

            String updatePets = "UPDATE `animalia`.`pets` SET `ownerId` = ?, `race` = ?, `birthDate` = ?, `isVaccinated` = ? WHERE (`petId` = ?)";

//            String updatePets = "UPDATE pets SET" + ownerId + "," + race + "," + birthDate + "," + isVaccinated + " WHERE (" + petId + ")";
            PreparedStatement stmt = conn.prepareStatement(updatePets);
            stmt.setInt(1, ownerId);
            stmt.setString(2, race);
            stmt.setString(3, birthDate);
            stmt.setBoolean(4, isVaccinated);
            stmt.setInt(5, petId);
            stmt.executeUpdate();
            stmt.close();
            conn.close();

        } catch (SQLDataException sqlException) {
            System.out.println(sqlException.getMessage());
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
}
